const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

let pluginsProuction = [];
if (process.env.NODE_ENV === 'production') {
    pluginsProuction.push(
        new HtmlWebpackPlugin({
            title: 'Ipex-ID',
            meta: { viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no' }
        })
    );
}

module.exports = {
    publicPath: '/login/',
    outputDir: path.resolve(__dirname, './login'),
    devServer: {
        overlay: {
            warnings: false,
            errors: false
        }
    },
    configureWebpack: {
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                'src': path.join(__dirname, './src'),
                'assets': path.join(__dirname, './src/assets'),
                'components': path.join(__dirname, 'src/components'),
                'utils': path.join(__dirname, 'src/utils'),
                'icons': path.join(__dirname, 'src/assets/img/icons'),
                'pages': path.join(__dirname, 'src/pages'),
                'const': path.join(__dirname, 'src/const'),
                'blocks': path.join(__dirname, 'src/assets/styles/blocks'),
                'mixins': path.join(__dirname, 'src/mixins')
            },
            extensions: ['*', '.js', '.vue', '.json']
        },
        plugins: [
            new webpack.DefinePlugin({
                LANGUAGE_DEFAULT: JSON.stringify('ru')
            }),
            new webpack.ProvidePlugin({
                axios: 'axios',
                mapGetters: ['vuex', 'mapGetters'],
                mapActions: ['vuex', 'mapActions'],
                mapMutations: ['vuex', 'mapMutations'],
                mapState: ['vuex', 'mapState']
            }),
            ...pluginsProuction
        ],
        optimization: {
            minimizer: [ new UglifyJsPlugin() ]
        }
    },
    css: {
        loaderOptions: {
            sass: {
                data: `@import "~assets/styles/common/variables.sass";`
            }
        }
    }
};
