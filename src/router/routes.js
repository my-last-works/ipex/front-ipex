import Home from 'pages/index';
import Login from 'pages/Login';
import LoginPartners from 'pages/Login/LoginPartners';
import Registration from 'pages/Registration';
import RegistrationVerify from 'pages/RegistrationVerify';
import PasswordRecovery from 'pages/PasswordRecovery';
import Profile from 'pages/Profile';
import Ois from 'pages/Ois';
import Upload from 'pages/Ois/Upload';
import Settings from 'pages/Profile/Settings';
import NotFoundPage from 'pages/NotFoundPage';

export default [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/registration',
        name: 'registration',
        component: Registration
    },
    {
        path: '/registration-verify',
        name: 'registration-verify',
        component: RegistrationVerify
    },
    {
        path: '/partner/nris',
        name: 'partner/nris',
        component: LoginPartners
    },
    {
        path: '/partner/sp',
        name: 'partner/sp',
        component: LoginPartners
    },
    {
        path: '/partner/zaycev',
        name: 'partner/zaycev',
        component: LoginPartners
    },
    {
        path: '/partner/fonmix',
        name: 'partner/fonmix',
        component: LoginPartners
    },
    {
        path: '/partner/ipchain',
        name: 'partner/ipchain',
        component: LoginPartners
    },
    {
        path: '/password-recovery',
        name: 'password-recovery',
        component: PasswordRecovery
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        meta: {
            auth: true
        }
    },
    {
        path: '/profile/settings',
        name: 'settings',
        component: Settings,
        meta: {
            auth: true
        }
    },
    {
        path: '/ois',
        name: 'ois',
        component: Ois,
        meta: {
            auth: true
        }
    },
    {
        path: '/ois/upload',
        name: 'oisUpload',
        component: Upload,
        meta: {
            auth: true
        }
    },
    {
        path: '*',
        component: NotFoundPage
    }
];
