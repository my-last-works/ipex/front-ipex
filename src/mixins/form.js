export default {
    data() {
        return {
            errorsForm: {}
        };
    },
    computed: {
        isValideForm() {
            return JSON.stringify({}) === JSON.stringify(this.errorsForm);
        }
    },
    methods: {
        clearErrorsForm() {
            this.errorsForm = {};
        },
        addErrorForm(key, text) {
            this.$set(this.errorsForm, key, text);
        },
        getErrorForm(key) {
            return this.errorsForm[key] || '';
        }
    }
};
