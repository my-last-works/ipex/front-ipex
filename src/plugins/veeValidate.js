import Vue from 'vue';
import VeeValidate from 'vee-validate';
import dictionary from './config/veeValidate/lang';
import { phoneOrEmailRule, password, repassword, phoneCode, seriesPassport, datePassport, codePassport, bDay } from './config/veeValidate/rules';

// VeeValidate.Validator.extend('required', required);
VeeValidate.Validator.extend('phoneOrEmail', phoneOrEmailRule);
VeeValidate.Validator.extend('password', password);
VeeValidate.Validator.extend('phoneCode', phoneCode);
VeeValidate.Validator.extend('repassword', repassword, {
    hasTarget: true
});
VeeValidate.Validator.extend('seriesPassport', seriesPassport);
VeeValidate.Validator.extend('datePassport', datePassport);
VeeValidate.Validator.extend('codePassport', codePassport);
VeeValidate.Validator.extend('bDay', bDay);

Vue.use(VeeValidate, {
    locale: LANGUAGE_DEFAULT,
    dictionary
});
