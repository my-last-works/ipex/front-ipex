import Vue from 'vue';
import store from '@/store';
import axios from 'axios';

class Http extends axios {
    static requests = {
        post: {
            headers: {
                'Accept': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            },
            withCredentials: true
        },
        get: {
            withCredentials: true
        }
    };

    static install(Vue, options) {
        Vue.prototype.$http = this;
    }

    static async get(...params) {
        const token = store.state.account.token;
        let [url, options = {}] = params;
        let { data, withCredentials, sendToken = false } = options;

        if (!withCredentials) options = Object.assign(options, this.requests.get);

        if (sendToken) {
            if (!token) await Vue.prototype.$awaitMutation('account/SET_TOKEN', 3000);

            if (!data) options['data'] = {};
            options.data = Object.assign(options.data, {
                _token: store.state.account.token
            });
            delete options.token;
        }

        return super.get(url, options);
    }

    static async post(...params) {
        const token = store.state.account.token;
        let [url, data = {}, options = {}] = params;
        let { headers, withCredentials } = options;

        if (!withCredentials) {
            options = Object.assign({}, {
                ...options,
                withCredentials: this.requests.post.withCredentials
            });
        }

        if (!headers) options = Object.assign(options, this.requests.post.headers);

        if (!token) await Vue.prototype.$awaitMutation('account/SET_TOKEN', 3000);

        if (data instanceof FormData) {
            data.append('_token', store.state.account.token);
        } else {
            data = Object.assign(data, {
                _token: store.state.account.token
            });
        }

        return super.post(url, data, options);
    }
}

Vue.use(Http);