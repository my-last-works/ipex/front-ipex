import Vue from 'vue';
import store from '@/store';

class AwaitMutation {
    static install(Vue) {
        Vue.prototype.$awaitMutation = function(typeMutation, time = 2000) {
            return new Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    subscribe();
                    reject(new Error(`Кончилось время ожидания мутации '${typeMutation}'`));
                }, time);
                const subscribe = store.subscribe((mutation, state) => {
                    if (mutation.type === typeMutation) {
                        subscribe();
                        resolve();
                        clearTimeout(timeout);
                    }
                });
            });
        };
    }
};

Vue.use(AwaitMutation);
