import Vue from 'vue';
import i18next from 'i18next';
import VueI18Next from '@panter/vue-i18next';

Vue.use(VueI18Next);

i18next.init({
    lng: LANGUAGE_DEFAULT,
    fallbackLng: 'en',
    resources: {
        'ru': {
            translation: require('utils/localization/ru.json')
        },
        'en': {
            translation: require('utils/localization/en.json')
        }
    }
});

export default new VueI18Next(i18next);
