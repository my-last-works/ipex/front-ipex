import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './plugins/awaitMutation';
import './plugins/autoloadBaseComponents';
import './plugins/veeValidate';
import './plugins/vuebar';
import './plugins/vueMask';
import './plugins/http';
import i18n from './plugins/i18n';
import { mapGetters } from 'vuex';
import toastr from 'toastr';

Vue.prototype.$toast = toastr;

Vue.config.productionTip = false;

Vue.mixin({
    computed: {
        ...mapGetters(['$loader'])
    }
});
// store.dispatch('setLoader', true);
// axios.get('https://id.test.ipex.global/api/profile', {withCredentials: true})
//     .then(({data}) => store.dispatch('account/loginUser', data))
//     .catch(() => store.dispatch('account/logoutUser'))
//     .finally(() => VUE());

// function VUE () {
//     const app = new Vue({
//         el: '#app',
//         components: { App },
//         template: '<App/>',
//         router,
//         store,
//         i18n
//     });
//
//     /**
//      * При работе на localhost добавим APP в глобальную видимость console браузера
//      */
//
//     window.location.hostname === 'localhost' ? window.APP = app : ''
// }

Vue.config.productionTip = false;
// Vue.use(store);
// Vue.use(VueMask);
// Vue.use(CoolLightBox);
// Vue.use(VueCarousel);

new Vue({
    router,
    i18n,
    store,
    render: h => h(App)
}).$mount('#app');
