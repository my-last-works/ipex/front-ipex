export default {
    namespaced: true,
    state: {
        pageComponent: 'registration-form',
        agreement: ''
    },
    getters: {},
    mutations: {
        SET_REGISTRATION_PAGE_COMPONENT(state, component) {
            state.pageComponent = component;
        },
        SET_REGISTRATION_AGREEMENT(state, agreement) {
            state.agreement = agreement;
        }
    },
    actions: {
        setRegistrationPageComponent({ commit }, component) {
            commit('SET_REGISTRATION_PAGE_COMPONENT', component);
        },
        setRegistrationAgreement({ commit }, agreement) {
            commit('SET_REGISTRATION_AGREEMENT', agreement);
        }
    }
};
