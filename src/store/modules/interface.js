export default {
    namespace: true,
    state: {
        loader: false
    },
    getters: {
        $loader(state) {
            return state.loader;
        }
    },
    actions: {
        setLoader({ commit }, boolean) {
            console.log('setLoader');
            commit('SET_LOADER', boolean);
        }
    },
    mutations: {
        SET_LOADER(state, boolean) {
            state.loader = boolean;
        }
    }
};
