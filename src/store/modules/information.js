export default {
    namespaced: true,
    state: {
        screen: null
    },
    getters: {
        getValueByProperty: state => property => {
            return state.screen && state.screen[property] ? state.screen[property] : null;
        }
    },
    mutations: {
        SET_SCREEN_PROPERTIES(state, screen) {
            state.screen = screen;
        }
    },
    actions: {
        setScreenProperties({ commit }, screen) {
            commit('SET_SCREEN_PROPERTIES', screen);
        }
    }
};
