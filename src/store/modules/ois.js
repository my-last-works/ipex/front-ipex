export default {
    namespaced: true,
    state: {
        loader: true,
        list: []
    },
    getters: {},
    mutations: {
        ASSIGN_CANDIDATES(state, candidates) {
            console.log('Итоговый результат: ', candidates);
            state.list = candidates;
            state.loader = false;
        }
    },
    actions: {
        getCandidates() {
            return new Promise((resolve) => axios.get('https://www.test.ipex.global/api/ipobjects', { withCredentials: true }).then(({ data }) => resolve(data)));
        },
        getIpchainCanditates() {
            return new Promise((resolve) => axios.get('https://www.test.ipex.global/api/ipcandidates', { withCredentials: true }).then(({ data }) => resolve(data)));
        },
        getListCandidates({ dispatch, commit }) {
            Promise.all([dispatch('getCandidates'), dispatch('getIpchainCanditates')]).then((res) => commit('ASSIGN_CANDIDATES', res));
        }
    }
};
