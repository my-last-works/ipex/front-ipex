import { API_TOKEN, API_PROFILE, API_LOGOUT } from 'const/api';

export default {
    namespaced: true,
    state: {
        isAuthorized: false,
        token: '',
        user: {}
    },
    getters: {
        getUserEmail(state) {
            return state.user.email || null;
        },
        isAuthorized: (state) => state.isAuthorized
    },
    mutations: {
        LOGOUT(state) {
            state.isAuthorized = false;
        },
        LOGIN(state) {
            state.isAuthorized = true;
        },
        SET_TOKEN(state, token) {
            state.token = token;
        },
        CHANGE_AUTHORIZED(state) {
            state.isAuthorized = !state.isAuthorized;
        },
        SET_USER_INFORMATION(state, payload) {
            state.user = payload;
        },
        CLEAR_USER_INFORMATION(state) {
            state.user = {};
        },
        LOGIN_USER(state, data) {
            state.user = data;
            state.isAuthorized = true;
        },
        LOGOUT_USER(state) {
            state.user = {};
            state.isAuthorized = false;
        }
    },
    actions: {
        async login({ commit, dispatch }) {
            await dispatch('getAndSetAccountInfo');
            commit('LOGIN');
        },
        generateToken({ commit }) {
            return axios.get(API_TOKEN, { withCredentials: true })
                .then(response => {
                    if (response.data && response.data.token) {
                        commit('SET_TOKEN', response.data.token);
                    }
                })
                .catch((error) => {
                    console.error('При получении token произошла ошибка!', error);
                });
        },
        tokenUpdateInterval({ dispatch }) {
            setTimeout(async () => {
                await dispatch('generateToken');
                dispatch('tokenUpdateInterval');
            }, 60000 * 9);
        },
        checkAuthorized({ dispatch }) {
            return axios.get(API_PROFILE, { withCredentials: true })
                .then(response => {
                    dispatch('changeAuthorized');
                    dispatch('setUserInformation', response.data);
                })
                .catch((error) => {
                    console.log('Error: ', error);
                });
        },
        getAndSetAccountInfo({ dispatch }) {
            return axios.get(API_PROFILE, { withCredentials: true })
                .then((response) => {
                    console.warn('getAndSetAccountInfo');
                    dispatch('setUserInformation', response.data);
                })
                .catch((error) => {
                    console.log('Error: ', error);
                });
        },
        changeAuthorized({ commit }) {
            commit('CHANGE_AUTHORIZED');
        },
        setUserInformation({ commit }, payload) {
            commit('SET_USER_INFORMATION', payload);
        },
        loginUser({ commit }, data) {
            commit('LOGIN_USER', data)
        },
        logout({ commit }) {
            return new Promise((resolve, reject) => {
                axios.get(API_LOGOUT, { withCredentials: true })
                    .then(() => resolve(commit('LOGOUT_USER')))
                    .catch((err) => reject(err));
            });
        },
        logoutUser({ commit }) {
            commit('LOGOUT_USER');
        }
    }
};
