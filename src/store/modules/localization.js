import { Validator } from 'vee-validate';

export default {
    namespaced: true,
    state: {
        language: LANGUAGE_DEFAULT
    },
    getters: {},
    mutations: {
        SET_LANGUAGE(state, lang) {
            state.language = lang;
        }
    },
    actions: {
        setLanguage({ commit }, lang) {
            commit('SET_LANGUAGE', lang);
            Validator.localize(lang);
        }
    }
};
