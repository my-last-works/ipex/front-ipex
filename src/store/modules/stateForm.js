export default {
    namespaced: true,
    state: {
        isDisabled: false
    },
    getters: {
        statusForm(state) {
            return state.isDisabled;
        }
    },
    mutations: {
        CHANGE_STATUS_FROM(state) {
            state.isDisabled = !state.isDisabled;
        }
    },
    actions: {
        changeStatusForm({ commit }) {
            commit('CHANGE_STATUS_FROM');
        }
    }
};
