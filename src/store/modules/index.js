import registration from './registration';
import login from './login';
import localization from './localization';
import stateForm from './stateForm';
import api from './api';
import information from './information';
import account from './account';
import face from './interface';
import ois from './ois';

export default {
    registration,
    login,
    localization,
    stateForm,
    api,
    information,
    account,
    interface: face,
    ois
};
