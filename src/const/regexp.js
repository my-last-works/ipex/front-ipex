export const REGEXP_PHONE = /^[\+]?[0-9\(\)\s\-]+$/;
export const REGEXP_EMAIL = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
export const REGEXP_PASSWORD = /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])/;
export const REGEXP_PHONE_CODE = /^[0-9]{6}$/;
export const REGEXP_SERIES_PASSPORT = /^[0-9 ]{5}?[0-9]{6}$/g; // @todo => разобраться потом, почему не работает сама ссылка серии паспорта
export const REGEXP_CODE_DIVISION = /^[0-9]{3}?[-]?[0-9]{3}$/g; // @todo => разобраться потом, почему не работает сама ссылка кода паспорта
