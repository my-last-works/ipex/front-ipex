const BASE_URL = 'https://www.dev.ipex.global/';
const DEV_URL = 'https://id.test.ipex.global/';

export const API_TOKEN = DEV_URL + 'api/token';
export const API_REGISTER = DEV_URL + 'register';
export const API_PHONE_VERIFY = DEV_URL + 'api/phone/verify';
export const API_PHONE_CONFIRM = DEV_URL + 'api/phone/confirm';
export const API_SUGGEST_EMAIL = BASE_URL + 'api/suggest/email';
export const API_REGISTER_VERIFY = DEV_URL + 'register/verify';
export const API_PROFILE = DEV_URL + 'api/profile';
export const API_LOGOUT = DEV_URL + 'logout';
export const API_BOOKMARKS_COUNT = BASE_URL + 'api/bookmarks/count';
